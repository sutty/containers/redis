FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache redis

COPY ./monit.conf /etc/monit.d/redis.conf

RUN sed -re "/^bind /d" -i /etc/redis.conf
RUN sed -re "/^protected-mode /d" -i /etc/redis.conf
RUN sed -re "/^logfile /d" -i /etc/redis.conf
RUN sed -re "s/^# (syslog-enabled).*/\1 yes/" -i /etc/redis.conf
RUN sed -re "s/^(appendonly).*/\1 yes/" -i /etc/redis.conf
RUN echo "pidfile /run/redis/redis.pid" >> /etc/redis.conf
RUN echo "daemonize yes" >> /etc/redis.conf
RUN echo "protected-mode no" >> /etc/redis.conf
RUN echo "maxmemory-policy allkeys-lfu" >> /etc/redis.conf
RUN echo "maxmemory 100mb" >> /etc/redis.conf

EXPOSE 6379
VOLUME "/var/lib/redis"
